{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    std.url = "github:divnix/std";
    std.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { std, ... }@ inputs:
    std.growOn
      {
        inherit inputs;
        cellsFrom = ./nix;
        cellBlocks = with std.blockTypes;[
          (runnables "apps")
          (devshells "shells")
          (nixago "configs")
        ];
      }
      {
        packages = std.harvest inputs.self [ "repo" "apps" ];
        devShells = std.harvest inputs.self [ "repo" "shells" ];
      };
}
