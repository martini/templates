{ inputs, cell }:
let
  inherit (inputs) nixpkgs std;
  lib = nixpkgs.lib // builtins;
in
lib.mapAttrs (_: std.lib.dev.mkShell) {
  default = {
    packages = [
      nixpkgs.nil
    ];
    nixago = [
      cell.configs.conform
      cell.configs.lefthook
      cell.configs.treefmt
    ];
  };
}
