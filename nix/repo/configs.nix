{ inputs, cell }:
let
  inherit (inputs) nixpkgs std;
  lib = nixpkgs.lib // builtins;
in
{
  conform = std.lib.cfg.conform {
    data = {
      commit.header.length = 79;
      commit.conventional.types = [
        "build"
        "chore"
        "ci"
        "docs"
        "feat"
        "fix"
        "perf"
        "refactor"
        "style"
        "test"
      ];
    };
  };
  lefthook = std.lib.cfg.lefthook {
    data = {
      commit-msg.commands = {
        conform.run = "${lib.getExe nixpkgs.conform} enforce --commit-msg-file {1}";
      };
      pre-commit.commands = {
        treefmt.run = "${lib.getExe nixpkgs.treefmt} {staged_files}";
      };
    };
  };
  treefmt = std.lib.cfg.treefmt {
    data = {
      formatter = {
        nix = {
          command = "nixpkgs-fmt";
          includes = [ "*.nix" ];
        };
      };
    };
    packages = [
      nixpkgs.nixpkgs-fmt
    ];
  };
}
